import { useState, createContext, useEffect } from 'react'

export const StyleContext = createContext(null)

export const StyleProvider = ({ children }) => {
  const [style, setStyle] = useState({
    backgroundColor: 'white',
    textColor: 'black'
  })

  useEffect(() => {
    console.log('style: ', style)
  }, [style])

  return (
    <StyleContext.Provider value={{ style, setStyle }}>
      {children}
    </StyleContext.Provider>
  )
}

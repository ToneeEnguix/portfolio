/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext } from 'react'
import { NavLink } from 'react-router-dom'
import { StyleContext } from '../context/StyleProvider'

export default function Navbar () {
  const { style } = useContext(StyleContext)
  const { backgroundColor, textColor } = style

  const mainStyle = {
    backgroundColor,
    color: textColor,
    a: {
      color: 'black'
    },
    '.active': {
      color: textColor
    }
  }

  return (
    <div css={mainStyle}>
      <NavLink
        to='/home'
        className={({ isActive }) => (isActive ? 'active' : undefined)}
      >
        Home
      </NavLink>
      <NavLink
        to='/settings'
        className={({ isActive }) => (isActive ? 'active' : undefined)}
      >
        Settings
      </NavLink>
    </div>
  )
}

import React from 'react'
import { useContext } from 'react'
import { StyleContext } from '../context/StyleProvider'

export default function ColorBox ({ x, i }) {
  const { style, setStyle } = useContext(StyleContext)

  const max = 255
  const min = 0

  const randomNum1 = React.useRef(
    Math.floor(Math.random() * (max - min + 1) + min)
  )
  const randomNum2 = React.useRef(
    Math.floor(Math.random() * (max - min + 1) + min)
  )
  const randomNum3 = React.useRef(
    Math.floor(Math.random() * (max - min + 1) + min)
  )

  return (
    <div
      onClick={() => {
        x === 'bg'
          ? setStyle({
              ...style,
              backgroundColor: `rgb(${randomNum1.current}, ${randomNum2.current}, ${randomNum3.current})`
            })
          : setStyle({
              ...style,
              textColor: `rgb(${randomNum1.current}, ${randomNum2.current}, ${randomNum3.current})`
            })
      }}
      style={{
        backgroundColor: `rgb(${randomNum1.current}, ${randomNum2.current}, ${randomNum3.current})`,
        height: '100px',
        width: '100px',
        border: '1px solid black',
        cursor: 'pointer'
      }}
    />
  )
}

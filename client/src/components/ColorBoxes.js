/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext, useEffect, useState } from 'react'
import { StyleContext } from '../context/StyleProvider'
import ColorBox from './ColorBox'

export default function ColorBoxes ({ title, x, textColor }) {
  const [update, setUpdate] = useState(false)

  useEffect(() => {
    update && setUpdate(!update)
  }, [update])

  const generateBoxes = () => {
    return [...Array(10).keys()].map((_, i) => <ColorBox key={i} x={x} />)
  }

  return (
    <div css={mainStyle}>
      <h3>{title}</h3>
      <div css={colorBoxesWrapper}>
        {!update ? generateBoxes() : <div style={{ height: '200px' }} />}
      </div>
      <Buttons
        x={x}
        update={update}
        setUpdate={setUpdate}
        textColor={textColor}
      />
    </div>
  )
}

const Buttons = ({ x, update, setUpdate, textColor }) => {
  const { style, setStyle } = useContext(StyleContext)

  const randomStyle = {
    height: '20px',
    width: '20px',
    border: '1px solid black',
    borderRadius: '4px',
    marginRight: '.5rem',
    ':active': {
      backgroundColor: textColor
    }
  }

  return (
    <div css={randomWrapper}>
      <div css={randomWrapper} onClick={() => setUpdate(!update)}>
        <div css={randomStyle} />
        <div>Randomise</div>
      </div>
      <div
        css={randomWrapper}
        onClick={() =>
          x === 'bg'
            ? setStyle({
                ...style,
                backgroundColor: 'white'
              })
            : setStyle({
                ...style,
                textColor: 'black'
              })
        }
      >
        <div style={{ marginLeft: '2rem' }} css={randomStyle} />
        <div>Default</div>
      </div>
    </div>
  )
}

const mainStyle = { paddingTop: '5rem' }

const colorBoxesWrapper = {
  display: 'flex',
  flexWrap: 'wrap',
  width: '502px',
  margin: '2rem auto 0',
  border: '1px solid black'
}

const randomWrapper = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  marginTop: '.5rem',
  cursor: 'pointer'
}

// para portfolio poner esto en sección settings?
// grid de 3x3 con borderRadius? probar si se puede con grid
// uno a cada lado en desktop y en columna para mobile
// potser fer comp per buttons

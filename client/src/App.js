// PACKAGES
import React from 'react'
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate
} from 'react-router-dom'
// CONTEXT
import { StyleProvider } from './context/StyleProvider'
// COMPONENTS / VIEWS
import Navbar from './components/Navbar.js'
import Home from './views/Home.js'
import Settings from './views/Settings.js'
// STYLES
import './App.css'

export default function App () {
  return (
    <StyleProvider>
      <Router>
        <Navbar />
        <Routes>
          <Route exact path='/home' element={<Home />} />
          <Route exact path='/settings' element={<Settings />} />
          <Route exact path='/' element={<Navigate to='/home' replace />} />
        </Routes>
      </Router>
    </StyleProvider>
  )
}

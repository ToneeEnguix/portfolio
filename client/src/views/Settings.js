/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'
import { useContext } from 'react'
import { StyleContext } from '../context/StyleProvider'
import ColorBoxes from '../components/ColorBoxes'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map(bp => `@media (min-width: ${bp}px)`))

export default function Home () {
  const { style } = useContext(StyleContext)
  const { backgroundColor, textColor } = style

  const mainStyle = mq({
    backgroundColor,
    color: textColor,
    textAlign: 'center',
    paddingBottom: '15rem'
  })

  return (
    <div css={mainStyle}>
      <ColorBoxes title={'Background Color'} x={'bg'} textColor={textColor} />
      <ColorBoxes title={'Text Color'} x={'text'} textColor={textColor} />
    </div>
  )
}
